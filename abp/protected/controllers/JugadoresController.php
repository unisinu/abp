<?php
class JugadoresController extends Controller
{
	public function actionIndex()
	{
	if(isset($_POST['Jugadores'])){
	
		//rint_r($_POST['Cliente']);
		$guardar = new Jugadores();
		$guardar->attributes = $_POST['Jugadores'];
	//	print_r($guardar);
		if($guardar->save()){
			//echo "guardo!";
		}
	}
		$modelo = new Jugadores();
		$this->render('index',array('modelo'=>$modelo));
	
	}
	public function actionMostrarJugadores(){
		
		//$modelo =  Equipos::model()->findAll();
		 $modelo = Yii::app()->db->createCommand('
		 SELECT jugadores.nombre,jugadores.posicion, jugadores.dorsal, jugadores.apellido, jugadores.Id, equipos.nombre As nombre_equipo
		FROM jugadores
		INNER JOIN equipos ON jugadores.equipo_id= equipos.id')->queryAll();
		$this->render('mostrar',array('modelo'=>$modelo));
	}
	
	public function actionEditar($id){
		
	//	echo $id;
		
		$guardar = Jugadores::model()->findByPk($id);
		if(isset($_POST['Jugadores'])){
			$guardar->attributes = $_POST['Jugadores'];
		
		if($guardar->save()){
			$this->redirect(array('mostrarJugadores'));
		}
	
	}

		$this->render('modificar',array('modelo'=>$guardar));

	}
	
	public function actionEliminar($id){
		try{
		$eliminar = Jugadores::model()->findByPk($id);
		$eliminar->delete();
		$this->redirect(array('mostrarJugadores'));
		}catch(Exception $e){
			
			$this->redirect(array('mostrarJugadores'));
		}
	}
}