<?php
class EquiposController extends Controller
{
	public function actionIndex()
	{
	if(isset($_POST['Equipos'])){
	
		//rint_r($_POST['Cliente']);
		$guardar = new Equipos();
		$guardar->attributes = $_POST['Equipos'];
	//	print_r($guardar);
		if($guardar->save()){
			//echo "guardo!";
		}
	}
		$modelo = new Equipos();
		$this->render('index',array('modelo'=>$modelo));
	
	}
	public function actionMostrarEquipos(){
		
		//$modelo =  Equipos::model()->findAll();
		 $modelo = Yii::app()->db->createCommand('
		 SELECT equipos.id, equipos.nombre, equipos.direccion, equipos.telefono, tecnicos.nombre As Tecnico
		 FROM equipos
		 INNER JOIN tecnicos ON equipos.tecnicos_Id = tecnicos.Id')->queryAll();
		$this->render('mostrar',array('modelo'=>$modelo));
	}
	
	public function actionEditar($id){
		
	//	echo $id;
		
		$guardar = Equipos::model()->findByPk($id);
		if(isset($_POST['Equipos'])){
			$guardar->attributes = $_POST['Equipos'];
		
		if($guardar->save()){
			$this->redirect(array('mostrarEquipos'));
		}
	
	}

		$this->render('modificar',array('modelo'=>$guardar));

	}
	
	public function actionEliminar($id){
		try{
		$eliminar = Equipos::model()->findByPk($id);
		$eliminar->delete();
		$this->redirect(array('mostrarEquipos'));
		}catch(Exception $e){
			
			$this->redirect(array('mostrarEquipos'));
		}
	}
}