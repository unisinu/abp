<?php
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
));?>

<h3>Modificando Jugador </h3>
<table border>
<tr>
<div>
<td><b><?php echo $form->labelEx($modelo,'nombre');?></b></td>
<td><?php echo $form->textField($modelo,'nombre');?></td>
<td><?php echo $form->error($modelo,'nombre');?></td>
</div>
</tr>

<tr>
<div>
<td><b><?php echo $form->labelEx($modelo,'apellido');?></b></td>
<td><?php echo $form->textField($modelo,'apellido');?></td>
<td><?php echo $form->error($modelo,'apellido');?></td>
</div>
</tr>
<tr>
<div>
<td><b><?php echo $form->labelEx($modelo,'posicion');?></b></td>
<td><?php echo $form->textField($modelo,'posicion');?></td>
<td><?php echo $form->error($modelo,'posicion');?></td>
</div>
</tr>
<tr>
<div>
<td><b><?php echo $form->labelEx($modelo,'dorsal');?></b></td>
<td><?php echo $form->textField($modelo,'dorsal');?></td>
<td><?php echo $form->error($modelo,'dorsal');?></td>
</div>
</tr>
<tr>
<div>
<td><b><?php echo $form->labelEx($modelo,'Equipo');?></b></td>
<td><?php echo CHtml::activeDropDownList($modelo, 'equipo_id', 
CHtml::listData(Equipos::model()->findAll(),'id', 'nombre')); ?></td>
<td><?php echo $form->error($modelo,'equipo_id');?></td>
</div>
</tr>

<div>
<tr>
<td>
<?php 
echo "<br>";
echo CHtml::submitButton('Guardar');?>
</td>
</tr>
</div>
</table>
<?php
$this->endWidget();
 ?>