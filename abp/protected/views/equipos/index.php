<?php
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
));?>

<h3>Nuevo Equipo -  <?php echo CHtml::link('Gestionar Equipos',array('mostrarEquipos'));?></h3>
<table border>
<tr>
<div>
<td><b><?php echo $form->labelEx($modelo,'nombre');?></b></td>
<td><?php echo $form->textField($modelo,'nombre');?></td>
<td><?php echo $form->error($modelo,'nombre');?></td>
</div>
</tr>

<tr>
<div>
<td><b><?php echo $form->labelEx($modelo,'direccion');?></b></td>
<td><?php echo $form->textField($modelo,'direccion');?></td>
<td><?php echo $form->error($modelo,'direccion');?></td>
</div>
</tr>
<tr>
<div>
<td><b><?php echo $form->labelEx($modelo,'telefono');?></b></td>
<td><?php echo $form->textField($modelo,'telefono');?></td>
<td><?php echo $form->error($modelo,'telefono');?></td>
</div>
</tr>

<tr>
<div>
<td><b><?php echo $form->labelEx($modelo,'Tecnicos');?></b></td>
<td><?php echo CHtml::activeDropDownList($modelo, 'tecnicos_Id', 
CHtml::listData(Tecnicos::model()->findAll(),'Id', 'nombre')); ?></td>
<td><?php echo $form->error($modelo,'tecnicos_Id');?></td>
</div>
</tr>

<div>
<tr>
<td>
<?php 
echo "<br>";
echo CHtml::submitButton('Guardar');?>
</td>
</tr>
</div>
</table>
<?php
$this->endWidget();
 ?>