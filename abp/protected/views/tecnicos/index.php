<?php
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
));?>

<h3>Nuevo Tecnico -  <?php echo CHtml::link('Gestionar Tecnico',array('mostrarTecnicos'));?></h3>
<table border>
<tr>
<div>
<td><b><?php echo $form->labelEx($modelo,'nombre');?></b></td>
<td><?php echo $form->textField($modelo,'nombre');?></td>
<td><?php echo $form->error($modelo,'nombre');?></td>
</div>
</tr>
<tr>
<div>
<td><b><?php echo $form->labelEx($modelo,'apellido');?></b></td>
<td><?php echo $form->textField($modelo,'apellido');?></td>
<td><?php echo $form->error($modelo,'apellido');?></td>
</div>
</tr>
<div>
<tr>
<td>
<?php 
echo "<br>";
echo CHtml::submitButton('Guardar');?>
</td>
</tr>
</div>
</table>
<?php
$this->endWidget();
 ?>